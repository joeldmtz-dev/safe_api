# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :safe_api,
  ecto_repos: [SafeApi.Repo]

# Configures the endpoint
config :safe_api, SafeApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rjOwCF1cl8qlvRwmLi6sSq/alyeBc8U5B6P7axEya9v6N7KiIxf4lZgi6RICmbgp",
  render_errors: [view: SafeApi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SafeApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

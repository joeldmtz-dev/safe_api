FROM msaraiva/elixir-dev

WORKDIR /app
COPY . /app

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix hex.info && \
    mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new.ez && \
    apk update && \
    apk add mysql-client && \
    mix deps.get

EXPOSE 4000
CMD ["/bin/sh", "-c", "mix ecto.create && mix ecto.migrate && mix phoenix.server"]

defmodule SafeApi.PageController do
  use SafeApi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
